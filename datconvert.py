#!/usr/bin/python3
# vim: ts=2:expandtab:sw=2:softtabstop=2:ai

import xml.sax
from mods.adfdb import *
import argparse
from sys import exit
from os import path, walk

class TosecHandler(xml.sax.ContentHandler):
  def __init(self):
    self.CurrentData=""

  def startElement(self, tag, attributes):
    self.CurrentData=tag
    if tag=="rom":
      curname=attributes["name"]
      cursize=attributes["size"]
      curhash=attributes["md5"]
      if str.lower(curname).endswith(".adf"):
        print("ADF: " + curname)
        try:
          db.putmeta(curname,cursize,curhash)
        except sqlite3.IntegrityError:
          print("Metas for " + curname + " already exist in DB")

  def endElement(self, tag):
    self.CurrentData=""

parser = argparse.ArgumentParser()
parser.add_argument("-f", metavar='filename', help="Tosec dat file", action="append")
parser.add_argument("--dir", help="Directory to parse", action="append")
parser.add_argument("-d", metavar='dbfile', help="DB file", required=True)
args=parser.parse_args()

if args.f == None and args.dir == None:
  print("You must specify either -f or --dir")
  exit(1)

dbfile=args.d

if not path.isfile(dbfile):
  print("The db doesn't exist yet, perhaps try initdb.py")
  exit(1)
db=adfdb(dbfile)

parser=xml.sax.make_parser()
parser.setFeature(xml.sax.handler.feature_namespaces, 0)

Handler=TosecHandler()
parser.setContentHandler(Handler)

listoffiles=[]
if args.f != None:
  listoffiles=args.f

if args.dir != None:
  for directory in args.dir:
    for root, subdirs, files in walk(directory):
      for filename in files:
        if str.lower(filename).endswith(".dat"):
          listoffiles+=[path.join(root,filename)]


for filename in listoffiles:
  if not path.isfile(filename):
    print("The file " + filename + " no longer exists.")
    continue
  parser.parse(filename)

db.commitdb()
db.closedb()
