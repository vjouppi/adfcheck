#!/usr/bin/python3
# vim: ts=2:expandtab:sw=2:softtabstop=2:ai

from sys import argv, exit
from os import path
from mods.adfile import *
from mods.adfdb import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-d", metavar='dbfile', help="DB file", required=True)
args=parser.parse_args()

dbfile=args.d
if path.isfile(dbfile):
  print("The db file " + dbfile + " already exists.")
  exit(1)

db=adfdb(dbfile)
db.initdb()
db.closedb()
