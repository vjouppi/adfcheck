#!/usr/bin/python3
# vim: ts=2:expandtab:sw=2:softtabstop=2:ai

from sys import exit, stdout, stderr
from os import path, rename, walk
from mods.adfile import *
from mods.stdhashes import *
from mods.colours import *
from mods.adfdb import *
import operator
import argparse


def addmatchperhash(curhash):
  if curhash not in matchesperhash:
    matchesperhash[curhash]=1
  else:
    matchesperhash[curhash]+=1

def matchentirefile():
  '''If the entire file matches, we don't have much to do'''
  global perfectmatch
  fullmatch=db.getadfname(adfhash)
  if fullmatch == "":
    print(red + "No match for entire MD5" + stdtrm)
  else:
    print("Found exact match: " + green + fullmatch + stdtrm)
    if args.r == True:
      filepath=path.dirname(filename)
      if filepath == "":
        filepath="."
      fqfile=path.join(filepath,fullmatch)
      print("Renaming to " + fqfile)
      rename(filename,fqfile)
    perfectmatch=True

  print("")

def matchbootblock():
  '''Boot block matching'''
  bbhash=adf.getbbhash()
  bbname=db.getbbname(bbhash)
  hashesforbb=db.gethashesforbb(bbhash)
  foundbbcount=len(hashesforbb)

  if bbname != "":
    print("Known boot block: " + cyan + bbname + stdtrm)
    if bbhash in stdboots:
      print(cyan + "This is a standard DOS boot block" + stdtrm)
  else:
    print(red + "Unknown boot block" + stdtrm)

  if foundbbcount > 10:
    print(yellow + "There are " + cyan + str(foundbbcount) + yellow + " matches, not listing matching filenames" + stdtrm)
  elif foundbbcount > 0:
    print(cyan + str(foundbbcount) + stdtrm + " filenames with matching boot block(s)")

  if foundbbcount > 0 and bbhash not in stdboots:
    matches=[] # temp var for adf hashes with matching boot blocks
    for adffromdb in hashesforbb:
      matches.append(adffromdb[0])
      if foundbbcount <= 10:
        print(green + db.getadfname(adffromdb[0]) + stdtrm)
      matchedtracks[0]=matches
      matchespertrack[0]=len(matchedtracks[0])
    for matchedbbhash in matchedtracks[0]:
      addmatchperhash(matchedbbhash)

  if foundbbcount == 0:
    print(red + "No match for boot block" + stdtrm)

def matchtracks():
  '''Track matching'''
  print("")
  print("Finding matches for tracks")
  print("Lower (0)   Upper (1)")
  for track in range(0,adf.maxtrack):
    matches=[] # temp var for adf hashes matching current track
    trkhash=adf.gettrkhash(track)

    if (track + 1) % 2 == 1:
      stdout.write('\x1b[11C')
    else:
      stdout.write('\x1b[12D')
    stdout.flush()

    if track > 159:
      stdout.write(yellow + "ADF contains over 159 tracks" + stdtrm)
      break

    if trkhash in emptytracks:
      foundempty.append(track)
      stdout.write(yellow + ".")
    else:
      hashesfromdb=db.gethashesfortrack(track,trkhash)
      if hashesfromdb == []:
        stdout.write(red + ".")
        pass
      else:
        stdout.write(green + ".")
        for hashfromdb in hashesfromdb:
          matches.append(hashfromdb[0])
          addmatchperhash(hashfromdb[0])
    stdout.flush()

    if (track + 1) % 20 == 0:
      print("")
    matchedtracks[track]=matches
    matchespertrack[track]=len(matchedtracks[track])

def findbestmatch():
  '''Find the file from the db which had the most matching tracks marked'''
  global sortedhashes
  global besthash
  global bestfile
  global bestmatches
  global nomatches

  sortedhashes=sorted(matchesperhash.items(), key=operator.itemgetter(1), reverse=True)
  if sortedhashes == []:
    print(red + "No matches for tracks found in the database" + stdtrm)
    nomatches=True
  if nomatches==False:
    besthash=sortedhashes[0][0] # hash of the best adf in the db
    bestmatches=sortedhashes[0][1] # amount of matching tracks for besthash
    bestfile=db.getadfname(besthash) # filename of the best adf in the db

def matchagainstbest():
  '''Match best file in db against image'''
  global confidence
  global trackmatches
  global tracknomatch

  bestimage=db.getallforhash(besthash)
  trackmatches=[]
  tracknomatch=[]
  # Boot block first
  if adf.getbbhash() != bestimage[3]:
    tracknomatch.append('bb')
    pass
  else:
    trackmatches.append('bb')
    confidence+=0.6211

  # Then the rest
  for track in range(0,adf.maxtrack+1):
    if track > 159:
      break
    if adf.gettrkhash(track) != bestimage[track+4]:
      tracknomatch.append(track)
      pass
    else:
      trackmatches.append(track)
      confidence+=0.6211

def showresults():
  if confidence > 74:
    confcol=green
  elif confidence < 75 and confidence > 25:
    confcol=yellow
  else:
    confcol=red
  print(stdtrm)
  print("Best match overall: " + confcol + bestfile + stdtrm)
  print("Confidence: " + confcol + str(confidence) + stdtrm + "% with " + confcol + str(bestmatches) + stdtrm + " matching non-empty tracks and " + confcol + str(len(trackmatches)) + stdtrm + " matching tracks total.")
  print("")
  stdout.write(stdtrm + "Empty tracks: " + yellow)
  print(foundempty)
  stdout.write(stdtrm + "No match for tracks: " + red)
  print(tracknomatch)
  stdout.write(stdtrm + "Matching tracks: " + green)
  print(trackmatches)
  print(stdtrm)
  runnersprinted=0
  for index in sortedhashes[1:]:
    if index[1] > 70:
      if runnersprinted == 0:
        print("Runners up with over 70 matching non-empty tracks:")
        runnersprinted=1
      #confidence=float(index[1]) / 161 * 100
      print(db.getadfname(index[0]) + " - " + str(index[1]))
  print("")

# main
parser = argparse.ArgumentParser()
parser.add_argument("-f", metavar='filename', help="Amiga disk file", action="append")
parser.add_argument("--dir", help="Directory to parse", action="append")
parser.add_argument("-d", metavar='dbfile', help="DB file", required=True)
parser.add_argument("-r", help="Rename exact matches from DB", action="store_true")
parser.add_argument("-c", help="Enable cache for DB", action="store_true")
args=parser.parse_args()

if args.f == None and args.dir == None:
  print("You must specify either -f or --dir")
  exit(1)

dbfile=args.d
if not path.isfile(dbfile):
  print("The db doesn't exist yet, perhaps try initdb.py")
  exit(1)
db=adfdb(dbfile)

listoffiles=[]
if args.f != None:
  listoffiles=args.f

if args.dir != None:
  for directory in args.dir:
    for root, subdirs, files in walk(directory):
      for filename in files:
        if str.lower(filename).endswith(".adf"):
          listoffiles+=[path.join(root,filename)]

if args.c == True:
  db.enablecache()

for filename in listoffiles:
  if not path.isfile(filename):
    print("The file " + filename + " no longer exists.")
    continue
  confidence=0
  perfectmatch=False
  nomatches=False
  matchedtracks={} #[track]: [hashes,of,images] - track 0 = bb 1 = 0 etc
  matchespertrack={} #[track]: number of matches
  matchesperhash={} #[hash]: number of matches
  foundempty=[] #track numbers found to be empty

  adf=adfile(filename)
  adfhash=adf.getadfhash()

  print ("Filename: " + cyan + filename + stdtrm)
  print ("File size: " + cyan + str(adf.adfsize) + stdtrm)
  print ("Volume label " + cyan + adf.getvolname() + stdtrm)
  print ("MD5: " + cyan + adfhash + stdtrm)

  matchentirefile()
  if perfectmatch == True:
    continue
  matchbootblock()
  matchtracks()
  findbestmatch()
  if nomatches == True:
    continue
  matchagainstbest()
  showresults()
  del(adf)
