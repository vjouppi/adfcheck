# ADF Hash

A set of python scripts for verifying ADFs track by track against a hash
database.

## Getting Started

Clone the repo, get a database from somewhere (or populate your own) and start
verifying disk images.

There is a starter.db included, which is prepopulated with hashes of a variety
of boot blocks.

./adfcheck.py -d dbfile -f adfile.adf

## More info

If you wish to create your own db, use initdb.py and then start adding your
boot block and disk images with hashbb.py and hashadf.py.

Adfcheck.py has a mem cache mode now, you can specify -c and the entire DB is
copied to RAM before processing begins. Perfect for batch mode.

Having your database under tmpfs might still be useful sometimes.

To help you get started with your database, a script called datconvert.py is
included. You can use this to import TOSEC DAT files into the database. Track
by track matching will naturally be missing, but at least you can get exact
matches.

The scripts have a batch mode, you can specify as many -f and --dir arguments
as you wish.

### Prerequisites

Python3 is required, also sqlite3.

If you wish to use the TOSEC DAT converter, then xml.sax is needed.

## Authors

* **Ville Jouppi** - *Initial work* - [vjouppi](https://bitbucket.org/vjouppi/)

## License

Copyright (C) 2019, Ville Jouppi <jope@jope.fi>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright owner nor the names of contributors to
this software may be used to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
