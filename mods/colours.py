from sys import modules

this = modules[__name__]
this.stdtrm="\x1b[0m"
this.red="\x1b[31m"
this.green="\x1b[32m"
this.yellow="\x1b[33m"
this.blue="\x1b[34m"
this.magenta="\x1b[35m"
this.cyan="\x1b[36m"
this.white="\x1b[37m"

