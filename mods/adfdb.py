# vim: ts=2:expandtab:sw=2:softtabstop=2:ai

import sqlite3

class adfdb:
  def __init__(self, dbfile):
    '''Connects to the db and creates a cursor cur'''
    self.db = sqlite3.connect(dbfile)
    self.cur = self.db.cursor()
    self.cache = False

  def getadfname(self, adfhash):
    '''Gets adf filename for hash of entire adf'''
    data = self.cur.execute("SELECT filename FROM meta WHERE hash=?", (adfhash,))
    result=data.fetchall()
    if (len(result) > 0):
      return result[0][0]
    else:
      return ""

  def getbbname(self, bbhash):
    '''Gets bootblock name for hash of bootblock'''
    data = self.cur.execute("SELECT bbname FROM bblocks WHERE hash=?", (bbhash,))
    result=data.fetchall()
    if (len(result) > 0):
      return result[0][0]
    else:
      return ""

  def gethashesforbb(self, bbhash):
    '''Gets hashes of all adfs with matching bootblock hash'''
    data = self.cur.execute("SELECT hash FROM hashes WHERE bblock=?", (bbhash,))
    return data.fetchall()

  def gethashesfortrack(self, currtrk, trkhash):
    '''Gets hashes of all adfs with matching track and track hash'''
    query = "SELECT hash FROM hashes WHERE trk" + str(currtrk) + "=?"
    data = self.cur.execute(query, (trkhash,))
    return data.fetchall()

  def getallforhash(self, adfhash):
    data = self.cur.execute("SELECT * FROM hashes WHERE hash=?", (adfhash,))
    return data.fetchall()[0]

  def initdb(self):
    if self.cache==False:
      self.cur.execute('CREATE TABLE bblocks (hash PRIMARY KEY, bbname);')
      self.cur.execute('CREATE TABLE meta (filename, filesize, hash PRIMARY KEY);')
      self.cur.execute('CREATE TABLE hashes (hash PRIMARY KEY, hashtype, volumename, bblock, trk0, trk1, trk2, trk3, trk4, trk5, trk6, trk7, trk8, trk9, trk10, trk11, trk12, trk13, trk14, trk15, trk16, trk17, trk18, trk19, trk20, trk21, trk22, trk23, trk24, trk25, trk26, trk27, trk28, trk29, trk30, trk31, trk32, trk33, trk34, trk35, trk36, trk37, trk38, trk39, trk40, trk41, trk42, trk43, trk44, trk45, trk46, trk47, trk48, trk49, trk50, trk51, trk52, trk53, trk54, trk55, trk56, trk57, trk58, trk59, trk60, trk61, trk62, trk63, trk64, trk65, trk66, trk67, trk68, trk69, trk70, trk71, trk72, trk73, trk74, trk75, trk76, trk77, trk78, trk79, trk80, trk81, trk82, trk83, trk84, trk85, trk86, trk87, trk88, trk89, trk90, trk91, trk92, trk93, trk94, trk95, trk96, trk97, trk98, trk99, trk100, trk101, trk102, trk103, trk104, trk105, trk106, trk107, trk108, trk109, trk110, trk111, trk112, trk113, trk114, trk115, trk116, trk117, trk118, trk119, trk120, trk121, trk122, trk123, trk124, trk125, trk126, trk127, trk128, trk129, trk130, trk131, trk132, trk133, trk134, trk135, trk136, trk137, trk138, trk139, trk140, trk141, trk142, trk143, trk144, trk145, trk146, trk147, trk148, trk149, trk150, trk151, trk152, trk153, trk154, trk155, trk156, trk157, trk158, trk159);')
    else:
      return False

  def putmeta(self, adfname, adfsize, adfhash):
    if self.cache==False:
      self.cur.execute('INSERT INTO meta VALUES (?,?,?)', (adfname,adfsize,adfhash))
      return True
    else:
      return False

  def puthashes(self, adfhash, volumename, hashes):
    if self.cache==False:
      self.cur.execute('INSERT INTO hashes values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', tuple([adfhash, 'md5', volumename]+hashes))
      return True
    else:
      return False

  def putbb(self, bbhash, filename):
    if self.cache==False:
      self.cur.execute('INSERT INTO bblocks VALUES (?,?)', (bbhash,filename))
      return True
    else:
      return False

  def commitdb(self):
    return self.db.commit()

  def closedb(self):
    return self.db.close()

  def enablecache(self):
    print("Caching DB to RAM, please hold..")
    self.memdb=sqlite3.connect(':memory:')
    memcur=self.memdb.cursor()
    for line in self.db.iterdump():
      memcur.execute(line)
    self.db.close()
    memcur.close()
    self.cur=self.memdb.cursor()
    self.cache=True
