# vim: ts=2:expandtab:sw=2:softtabstop=2:ai

from hashlib import md5
from os import path

class adfile:
  def __init__(self,adfile):
    adfh = open(adfile,'rb')
    self.adfsize = path.getsize(adfile)
    self.hashes=[]
    track=0
    while adfh.tell() < self.adfsize:
      trkdata=adfh.read(11*512)
      if track == 0:
        self.hashes.append(md5(trkdata[0:1024]).hexdigest())
        self.hashes.append(md5(trkdata[1024:]).hexdigest())
      else:
        self.hashes.append(md5(trkdata).hexdigest())
      if track == 80:
        if trkdata[0:4] == b"\00\00\00\02": #lazy root block matching
          fnlen=trkdata[432]
          if fnlen <= 30:
            self.volumename=trkdata[433:433+fnlen].decode('latin-1')
        else:
          self.volumename='NDOS'
      track+=1

    adfh.seek(0)
    self.adfhash=md5(adfh.read()).hexdigest()
    adfh.close()
    self.maxtrack=len(self.hashes)-1

  def getbbhash(self):
    return self.hashes[0]

  def gettrkhash(self, trk):
    trk+=1
    return self.hashes[trk]

  def getvolname(self):
    return self.volumename

  def getadfhash(self):
    return self.adfhash
