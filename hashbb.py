#!/usr/bin/python3
# vim: ts=2:expandtab:sw=2:softtabstop=2:ai

from sys import argv, exit
from os import path, walk
from mods.adfile import *
from mods.adfdb import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", metavar='filename', help="Boot block file", action="append")
parser.add_argument("--dir", help="Directory to parse", action="append")
parser.add_argument("-d", metavar='dbfile', help="DB file", required=True)
args=parser.parse_args()

if args.f == None and args.dir == None:
  print("You must specify either -f or --dir")
  exit(1)

dbfile=args.d

if not path.isfile(dbfile):
  print("The db doesn't exist yet, perhaps try initdb.py")
  exit(1)
db=adfdb(dbfile)

listoffiles=[]
if args.f != None:
  listoffiles=args.f

if args.dir != None:
  for directory in args.dir:
    for root, subdirs, files in walk(directory):
      for filename in files:
        listoffiles+=[path.join(root,filename)]

for filename in listoffiles:
  if not path.isfile(filename):
    print("The file " + filename + " no longer exists.")
    continue
  basefile=path.basename(filename)
  bbfile=open(filename,'rb')
  bbsize=path.getsize(filename)

  if bbsize == 1024:
    print("Adding bb: " + filename)
    bbhash=md5(bbfile.read()).hexdigest()
    try:
      db.putbb(bbhash,basefile)
    except sqlite3.IntegrityError:
      print(filename + " already exists in db")
      continue

db.commitdb()
db.closedb()
